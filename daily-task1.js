// 1. Buat arraay of object students ada 5 students cukup
// 2. 3 function, 1 function nentuin province kalian ada di jawa barat apa nggak, age kalian 22 atau nggak, status kalian single atau nggak
// 3. callback function untuk print hasil proses 3 function di atas


let students = [
    {
        name: 'Rizki',
        age: 22,
        province: 'Jawa Tengah',
        status: 'Have a ayang'
    },
    {
        name: 'Rizka',
        age: 17,
        province: 'Jawa Barat',
        status: 'Single'
    },
    {
        name: 'Rizku',
        age: 20,
        province: 'Jawa Timur',
        status: 'Single'
    },
    {
        name: 'Rizke',
        age: 21,
        province: 'Papua',
        status: 'Single'
    },
    {
        name: 'Rizko',
        age: 24,
        province: 'Sumatra Barat',
        status: 'Single'
    }
]

// x is integer, dataStudents is array

function provinceJabar(x, dataStudents) {
    if (dataStudents[x].province != 'Jawa Barat') {
        return dataStudents[x].province
    } else {
        return 'Jawa Barat'
    }
}

function old(x, dataStudents) {
    if (dataStudents[x].age != 22) {
        return `not 22 but ${dataStudents[x].age}`
    } else {
        return '22'
    }
}

function statusStudents(x, dataStudents) {
    if (dataStudents[x].status != 'Single') {
        return dataStudents[x].status
    } else {
        return 'Single'
    }
}

function print(dataStudents, callback) {
    for (let i = 0; i < dataStudents.length; i++) {
        callback(dataStudents[i].name, provinceJabar(i, dataStudents), old(i, dataStudents), statusStudents(i, dataStudents));
    }

}

print(students, (name, province, age, status) => {
    console.log(`My name is ${name}, i'm from ${province}, i'm ${age} years old. My status is ${status}`);
})